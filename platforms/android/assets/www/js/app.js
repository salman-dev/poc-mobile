// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.payPalService', 'starter.paymentService'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})


.constant('shopSettings',{

  payPalSandboxId :'AdI4rAsugDk-8C-sLhS_66oS0Mw4iTZEVTBtDW3fysQDSTXa1ZnXKDSim2Qo82ruR0QIz62dcvmUu_6Q',

  payPalProductionId : 'production id here',

  payPalEnv: 'PayPalEnvironmentSandbox', // for testing production for production

  payPalShopName : 'poc',

  payPalMerchantPrivacyPolicyURL : 'url to policy',

  payPalMerchantUserAgreementURL : 'url to user agreement'

})

.constant('userConstant', {
  'userId' : 'USR123',
  'userName' : 'Virat',
  'userAddress' : '14 Crouton House, NJ'
})

.constant('sellerConstant', {
  'sellerId' : 'SLR123',
  'sellerName' : 'Black Diamond Games',
  'sellerAddress' : '1950 Market St',
  'commission' : 0.05
})

.constant('productConstant', {
  productId : 'PRO123',
  productName : 'Sony Playstation 4',
  productDetails : [
    'USB 3.0',
    'Ethernet Connection',
    'HDMI Output',
    '500GB Hard Disk'
  ],
  productImage : 'http://s3.amazonaws.com/digitaltrends-uploads-prod/2013/10/Sony-Playstation-4-front-kit-macro.jpg',
  productPrice: 348.0
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

    .state('success', {
    url: "/success",
    templateUrl: "templates/successTemplate.html",
    controller:'PurchaseCtrl'
  })



  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'PurchaseCtrl'
      }
    }
  })

  .state('invoice', {
    url: '/invoice',
    templateUrl: 'templates/tab-invoice.html',
    controller: 'InvoiceCtrl',
    params: {
      result: null
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
