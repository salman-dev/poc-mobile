/**
 * Created by Salman on 5/19/2016.
 */

var app = angular.module('starter.paymentService', [])
.service('PaymentService', function($q, $http) {
    return {
        savePayment: function(paymentData) {
            var deferred = $q.defer();

            var request = $http({
                method : 'post',
                url : 'https://mean-poc-api.herokuapp.com/api/payment',
                data : JSON.stringify(paymentData)
            });

            request.success(function(data) {
                deferred.resolve(data);
            });

            request.error(function(data) {
               deferred.reject(data);
            });
            return deferred.promise;
        }
    }
});
