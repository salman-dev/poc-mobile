angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $state) {
    $scope.data = {};

    $scope.login = function() {
        LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
            $state.go('tab.dash');
        }).error(function(data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
        });
    }
})

.controller('DashCtrl', function($scope) {})

.controller('PurchaseCtrl', function($scope, $state, $ionicHistory, userConstant,sellerConstant, productConstant,PaypalService, PaymentService){
	
    $scope.userName = userConstant.userName;
    $scope.sellerName = sellerConstant.sellerName;
    $scope.sellerAddress = sellerConstant.sellerAddress;
    $scope.productName = productConstant.productName;
    $scope.productDetails = productConstant.productDetails;
    $scope.productPrice = productConstant.productPrice;
    $scope.productImage = productConstant.productImage;
    var systemCommission = productConstant.productPrice * sellerConstant.commission;
    $scope.purchase = function() {
        PaypalService.initPaymentUI().then(function () {

            PaypalService.makePayment(productConstant.productPrice, "Total Amount").then(function (response) {

                var paymentData = {
                    paymentId : response.response.id,
                    paymentState : response.response.state,
                    userId : userConstant.userId,
                    userName : userConstant.userName,
                    userAddress : userConstant.userAddress,
                    productId : productConstant.productId,
                    productPrice : productConstant.productPrice,
                    commission : systemCommission
                };
                PaymentService.savePayment(paymentData)
                    .then(function (result) {
                        alert(result.message);
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('invoice', {result:paymentData});
                    }, function (error) {
                        alert(error.message);
                    });
            }, function (error) {
                alert("Transaction Failed: " +error);
            });

        });
    }	

})

.controller('InvoiceCtrl', function($scope, $state, $stateParams, sellerConstant, productConstant) {

    var data = $stateParams.result;

    $scope.paymentId = data.paymentId;
    $scope.paymentState = data.paymentState;
    $scope.userName = data.userName;
    $scope.sellerName = sellerConstant.sellerName;
    $scope.sellerAddress = sellerConstant.sellerAddress;
    $scope.productName = productConstant.productName;
    $scope.productImage = productConstant.productImage;
    $scope.productDetails = productConstant.productDetails;
    $scope.productPrice = data.productPrice;
})


.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
});


