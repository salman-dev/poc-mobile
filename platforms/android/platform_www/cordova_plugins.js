cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/card.io.cordova.mobilesdk/www/cdv-plugin-card-io.js",
        "id": "card.io.cordova.mobilesdk.CardIO",
        "clobbers": [
            "CardIO"
        ]
    },
    {
        "file": "plugins/com.paypal.cordova.mobilesdk/www/cdv-plugin-paypal-mobile-sdk.js",
        "id": "com.paypal.cordova.mobilesdk.PayPalMobile",
        "clobbers": [
            "PayPalMobile"
        ]
    },
    {
        "file": "plugins/org.pbernasconi.progressindicator/www/progressIndicator.js",
        "id": "org.pbernasconi.progressindicator.ProgressIndicator",
        "clobbers": [
            "ProgressIndicator"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "card.io.cordova.mobilesdk": "2.0.1",
    "com.paypal.cordova.mobilesdk": "3.2.0",
    "cordova-plugin-whitelist": "1.2.2",
    "org.pbernasconi.progressindicator": "1.1.0"
};
// BOTTOM OF METADATA
});